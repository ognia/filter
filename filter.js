define(['jquery', 'slider'],
function application($, Slider) {
    'use strict';

    var Filter = function(opts) {
        this.form          = opts.form;
        this.action        = this.form.attr('action');
        this.slider        = this.form.find('.slider-range');
        this.activeElement = {};
    };

    var pt = Filter.prototype;

    pt.render = function() {
        var self = this;

        this.slider.each(function() {
            var $s    = $(this);
            var $wrap = $s.closest('.slider');

            var slider = new Slider();

            slider.render({
                slider   : $s,
                cont     : $wrap,
                step     : $s.data('step'),
                min      : $s.data('min'),
                max      : $s.data('max'),
                valueMin : $wrap.find('.slider-min-inp').val(),
                valueMax : $wrap.find('.slider-max-inp').val(),

                changeSlider: function(el) {
                    self.activeElement = el;
                    self._removeSomeFilelds();
                    self._send();
                }
            });
            $s.data('slider', slider);
        });
    };

    return Filter;
});
