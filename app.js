define(['jquery'], function application($) {
    'use strict';

    // поиск по параметрам
    (function($filter) {
        if (!$filter.length) {
            return;
        }

        require(['filter'], function(Filter) {
            var filter = new Filter({
                form: $filter
            });
            filter.render();
        });
    })($('.filter-js'));
});
