define(['jquery', 'jquery-ui/slider', 'jquery-ui/touch-punch'],
function application($) {
    'use strict';

    var Slider = function() {
    	this.scopeClass = 'slider-scope';
    	this.scopeTpl   = $('<span class="' + this.scopeClass + '"></span>');
    };

    var pt = Slider.prototype;

    pt.render = function(params) {
        var self     = this;

        params       = params || {};
        this.slider  = params.slider;

        this.min     = params.min  || 0;
        this.max     = params.max  || 100;
        this.step    = params.step || 1;

        var $cont    = params.cont;

        this.inpMin  = $cont.find('.slider-min-inp');
        this.inpMax  = $cont.find('.slider-max-inp');

        var valueMin = toInt(params.valueMin) || this.min;
        var valueMax = toInt(params.valueMax) || this.max;

        var callbackSlider = function callbackSlider() {
            if (typeof(params.changeSlider) !== 'function') {
                return;
            }

            params.changeSlider(self.slider, self.sliderScope);
        };

        this.slider.slider({
            range : true,
            min   : this.min,
            max   : this.max,
            step  : this.step,
            values: [valueMin, valueMax],

            slide: function(event, ui) {
                self.setInputValues(ui.values[0], ui.values[1]);
            },
            stop: function() {
                callbackSlider();
            }
        });

        this.setInputValues(valueMin, valueMax);

        this.inpMin.add(this.inpMax).on('change', function() {
            self.checkBounds(self.slider, this);
            callbackSlider();
        });
        this.sliderScope([this.min, this.max], this.slider);
    };

    pt.checkBounds = function($slider) {
        var self   = this;
        var minVal = toInt(this.inpMin.val());
        var maxVal = toInt(this.inpMax.val());

        // не меньше минимально допустимого и не больше максимально допустимого
        var noMinMax = Math.min(Math.max(self.min, minVal), this.max);
        // не больше максимально допустимого и не меньше минимально допустимого
        var noMaxMin = Math.max(Math.min(self.max, maxVal), this.min);

        var resultMin = Math.min(noMinMax, noMaxMin);
        var resultMax = Math.max(noMinMax, noMaxMin);

        this.setInputValues(resultMin, resultMax);

        $slider.slider('values', [resultMin, resultMax]);
    };

    pt.setDefault = function($slider) {
        this.setInputValues(this.min, this.max);

        $slider.slider('values', [this.min, this.max]);
    };

    pt.setInputValues = function(minVal, maxVal) {
        this.inpMin.val(numberFormat(minVal));
        this.inpMax.val(numberFormat(maxVal));
    };
    /*Рисуем диапазон в котором есть квартиры удовлетворяющие результатам
        полоска голубого цвета на слайдере
    */
    pt.sliderScope = function(data, $slider) {
        var $sliderScope = $slider.find('.' + this.scopeClass);
        var $scope       = $sliderScope.length ? $sliderScope : this.scopeTpl;

        var step  = $slider.data('step');
        var min   = $slider.data('min');
        var max   = $slider.data('max');

        /*считаем ширину одного шага*/
        var rangeStep = (100 * step) / (max - min);
        /*считаем начало*/
        var minScope  = (data[0] - min) / step * rangeStep;
        /*считаем конец*/
        var maxScope  = (max - data[1]) / step * rangeStep;

        if (!$sliderScope.length) {
            $slider.append($scope);
        }

        $scope.css({
            left : minScope + '%',
            right: maxScope + '%'
        });
    };

    var toInt = function toInt(str) {
        var val = str.toString().replace(/[^0-9]/g, '');

        val = parseInt(val, 10) || 0;
        return val;
    };

    var numberFormat = function numberFormat(str) {
        var d = str.toString();

        if (d.length > 3) {
            d = d.replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
        }
        return d;
    };

    return Slider;
});
